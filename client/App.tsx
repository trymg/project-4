// import './App.css';
import Header from './src/components/Header';
import Movies from './src/components/Movies';

import { MoviesStore } from './src/components/MoviesStore';

import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';

import useCachedResources from './src/hooks/useCachedResources';
import useColorScheme from './src/hooks/useColorScheme';
import Navigation from './src/navigation';
import { View } from './src/components/Themed';
import {Text} from 'react-native'

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();


  if (!isLoadingComplete) {
    return null;
  } else {
    return (
          <SafeAreaView>
            {/* <Navigation colorScheme={colorScheme} />
            <StatusBar /> */}
              
                {/* <Text>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </Text> */}
              <Header moviesStore={MoviesStore}/>
              <Movies moviesStore={MoviesStore}/>
          </SafeAreaView>
    );
  }
}
