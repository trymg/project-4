import { action, makeObservable, observable } from "mobx";

// implements global state using MobX library
export class MoviesStoreImplementation {

    movies: Array<object> = [];
    hasMore: boolean = true;
    
    constructor() {
        makeObservable(this, {
            movies: observable,
            setMovies: action,
            hasMore: observable,
            setHasMore: action
        });
        
    }

    setMovies(movies: any) {
        this.movies = movies;
    }

    setHasMore(hasMore: boolean) {
        this.hasMore = hasMore;
    }
}

export const MoviesStore = new MoviesStoreImplementation();