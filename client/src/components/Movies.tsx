import {useEffect} from 'react';
import PrintTable from "./PrintTable";
import InfiniteScroll from "react-infinite-scroller";
import { MoviesStore, MoviesStoreImplementation } from "./MoviesStore";
import { inject, observer } from 'mobx-react';
import * as services from './services';


interface MoviesProps {
    moviesStore: MoviesStoreImplementation
}

// returns printTable with pagination. gets all movies by default, otherwise uses search results
function Movies(props: MoviesProps){

    useEffect(() => {
        if (MoviesStore.hasMore){
            getAllMovies({lim: 13});
        }
    }, []);


    // returns all movies from backend
    function getAllMovies(params: any) {
        services.queryFetch(services.generateAllMoviesQuery(params.lim))
        .then(res => res.json())
        // .then(res => setMovies(res.data.getAllMovies))
        .then(res => {
            props.moviesStore.setMovies(res.data.getAllMovies);
            props.moviesStore.setHasMore(true);
        })
    }

    // returns movies by given field (Abbasj:This method became retired by other students in last week!)
    function getMovieByColumnType(columnType : services.ColumnType, value: string, neededData: string) {
        services.queryFetch(services.generateQuery(columnType, value, neededData))
        .then(res => res.json())
        // .then(res => setMovies(res.data.getMovieByColumnType))
        .then(res => props.moviesStore.setMovies(res.data.getMovieByColumnType))
    }


    return (
        <InfiniteScroll
            pageStart={0}
            loadMore={() => {
                if (MoviesStore.hasMore){
                    getAllMovies({ lim: MoviesStore.movies.length + 13 })
                }
            }}
            hasMore={MoviesStore.hasMore}
            useWindow={true}

            loader={ 
                <div key="loading" className="loader">
                    Loading ...
                </div>
            }
        >
        <PrintTable data={MoviesStore.movies}/>
        </InfiniteScroll>
    );
}

export default inject('moviesStore')(observer(Movies))
