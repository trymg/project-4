// Returns list of movies
function PrintTable(props: any) {
    return (
        <div id="tableContainer"><table className="table-responsive">
            <thead>
            <tr>
                <th>Title</th>
                <th>Genre</th>
                <th>Year released</th>
                <th>Director</th>
                <th>Description</th>
                <th>Thumbnail</th>
            </tr>
            </thead>
            <tbody>{
                props.data.map((row: any) => {
                    return <tr key={row.title}>
                        <td>{row.title}</td>
                        <td>{row.genre}</td>
                        <td>{row.year}</td>
                        <td>{row.director_first_name + ' ' + row.director_last_name}</td>
                        <td>{row.description}</td>
                        <td><img className="thumbnails" src = {row.cover_image} alt={row.title}></img></td>
                    </tr>;  
                })
            }</tbody></table></div>
    )
}

export default PrintTable;
