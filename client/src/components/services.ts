// used to send all requests to backend
export function queryFetch(query: String) {
    return fetch('/graphql', {
        method: 'POST',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
        query: query,
        })
    })
}

export enum ColumnType{
    Id,
    Title, 
    Genre,
    Rating,
    Year,
    DirectorFirstName,
    DirectorLastName,
    Description
}

// generates GraphQL query for getting all movies
export function generateAllMoviesQuery(lim: number){
        return `
            query {
                getAllMovies(lim: ${lim}) {
                    title,genre,year,director_first_name,director_last_name,description,cover_image
                }
            }`
    }


// generates custom GraphQL query based on field, value of given field, and what data is needed
export function generateQuery(columnType: ColumnType, value: string, neededData: string){
    let column = '';

    switch(columnType){
        case ColumnType.Title:
            column = 'title';
            break;
        case ColumnType.Genre:
            column = 'genre';
            break;
        case ColumnType.Rating:
            column = 'rating_dice_throw';
            break;
        case ColumnType.Year:
            column = 'year';
            break;
        case ColumnType.DirectorFirstName:
            column = 'director_first_name';
            break;
        case ColumnType.DirectorLastName:
            column = 'director_last_name';
            break;
        case ColumnType.Description:
            column = 'description';
            break;
        default:
            column = 'id';
            break;
    }

    return `
    query {
        getMovieByColumnType(${column}: "${value}") {
            ${neededData}
        }
    }`;
}

// generates specialized GraphQL query for search
export function generateSearchQuery(neededData: string, title: string, genre: string, order: boolean){
    let query = `
    query {
        getMoviesBySearch(title: "${title}", genre: "${genre}", order: ${order}) {
            ${neededData}
        }
    }`;
    console.log(query)
    return query
}
