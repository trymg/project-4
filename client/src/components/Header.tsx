import React from 'react';
import { MoviesStoreImplementation } from "./MoviesStore";
import { inject, observer } from 'mobx-react';
import * as services from './services';
import {View, Text, TextInput, Button} from 'react-native';
// import CheckBox from '@react-native-community/checkbox';

type MyState = {
    inputTitle: string,
    inputGenre: string,
    checkbox1_checked: boolean
};

interface MoviesProps {
    moviesStore: MoviesStoreImplementation
}

// header containing search field with filtering options
class Header extends React.Component<MoviesProps, MyState> {
    constructor(props: any) {
        super(props);

        this.state = {
            inputTitle: '',
            inputGenre: '',
            checkbox1_checked: false
        }
    }


    render() {
        return (
            <View> 
                <Text>Movie title:</Text>
                <TextInput placeholder="Search for movie titles..." onChange={this.handleChange} nativeID="textfield1"></TextInput>
                <Text>Movie genre:</Text>
                <TextInput placeholder="Filter on genre..." onChange={this.handleChange} nativeID="textfield2"></TextInput>
                <Text>Order by year:</Text>
                {/* <CheckBox nativeID="checkbox1" onValueChange={this.handleChange}></CheckBox> */}
                <Button onPress={this.search} title="Search" />
            </View>
        )
    }

    // sets state based on input from user
    handleChange = (event: any) => {
        let element = event.target.id
        
        if (element === 'textfield1') {
            this.setState({ inputTitle: event.target.value })
        }
        if (element === 'textfield2') {
            this.setState({ inputGenre: event.target.value })
        }
        else if (element === 'checkbox1') {
            this.setState({ checkbox1_checked: event.target.checked })
        }
    }

    search = () => {
        this.getMoviesBySearch('title genre year director_first_name director_last_name description cover_image', this.state.inputTitle, this.state.inputGenre, this.state.checkbox1_checked);
    }

    // returns movies by given field
    getMovieByColumnType(columnType : services.ColumnType, value: string, neededData: string) {
        services.queryFetch(services.generateQuery(columnType, value, neededData))
        .then(res => res.json())
        .then(res => {
            this.props.moviesStore.setHasMore(false);
            this.props.moviesStore.setMovies(res.data.getMovieByColumnType);
        })
    }

    // returns movies using search filtering
    getMoviesBySearch(neededData: string, title: string, genre: string, order: boolean) {
        services.queryFetch(services.generateSearchQuery(neededData, title, genre, order))
        .then(res => res.json())
        .then(res => {
            this.props.moviesStore.setHasMore(false);
            this.props.moviesStore.setMovies(res.data.getMoviesBySearch);
        })
    }
}


export default inject('moviesStore')(observer(Header))