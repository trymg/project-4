# URLs

## The URL of the Client app
http://it2810-54.idi.ntnu.no/

*OBS: you need to be connected to NTNU VPN or use NTNU Network

## Timesheet
The timesheet is uploaded on Blackboard too.<br>
https://docs.google.com/spreadsheets/d/1Nlrbq4YBkYUjgJGk-ZNfBDjagppEqsFC/edit#gid=2013650662

# About the app
Our application is a web based solution for storing and searching through a large set of different movies.

## Project setup
We started by installing Node and npm, were Node 14.X and npm 6.X was required.
Then, the project setup was done simply using [Create React App](https://github.com/facebook/create-react-app):
```bash
create-react-app project-3 --template typescript
```
This was very convenient not having to set everything up manually. It also fulfilled a requirement of using both React and Typescript for static typechecking.
Then, we devided the project into two subprojects - 'client' and 'server'. This allowed for clear separation when working with either frontend or backend.
To add the new project files to the repository, we installed git as required, added the remote, and pushed the project. 

## Backend
GraphQL was used for the backend as it was required. The resolvers here would query on to the database.
For the database management system, we used MySQL, as we were already familiar with using it, and had an assigned database served by NTNU. For the dataset, we used [mockaroo.com](http://mockaroo.com), which can be used to generate datasets of any kind in any format. At first, we tested this locally by exporting the dataset in a JSON format, and eventually exported the dataset as a MySQL script that could be executed to fill up the database. This allowed us to test the application for large datasets as the website can generate datasets of any size. 

To prevent overloading data from the backend, we used a limit in the client by using pagination ? @thomas

## Frontend
For the client, we used the JavaScript Fetch API in order to fetch data from the /graphql endpoint on the server. This worked well by first generating a query based on what was needed in the client, and passing it to the fetch function. The result was then updated in the state.
MobX was used to for managing a global state. The choice of using MobX was based on the fact that it is regarded as easy to use compared to other Libraries like Redux, as it has more abstraction, and allows for simpler and overall less code. We used this to share movies between the search function and the Movies component that displays them to the user. This meant that whenever a search was made, the Movies component would automatically update with a new list of movies.

The website was made using multiple different components composed together on one page. A responsive layout was made possible using flexbox in css. The main components are the "Movies" and the "Header" components. Movies contains a list veiw of all the movies retreived. Header is at the top, with a search bar along with filtering options.
We tried to design it as simple and intuitively as possible so that any user will be able to use the website.

## Deployment
We deployed the client APP into NTNUs users server by createing an Apache server and pushed the build folder directly to /tmp folder (Using SSH) and then copied to /var/www/html/ folder. For the server, we have used "forever" for running the server. We manualy uploaded server files to ntnuhome folder and then by "tsc src/server.ts" tried to build/convert the .ts files to .js files. Finally "forever start src/server.js" has started the server.
*OBS: On remote server, for starting the server change ("start": "nodemon src/server_local.ts --exec ts-node") with ("start": "forever start src/server.ts") in package.json

## Available Scripts

In each of the project directories 'client' and 'server', you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
