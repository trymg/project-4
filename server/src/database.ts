import { createPool , Pool} from "mysql2/promise";

var _connection = connect();


 async function connect(): Promise<Pool>{

   if(typeof(_connection) === "undefined"){

      const connection = createPool({
        host: "mysql.ansatt.ntnu.no",
        user: 'abbasj_abbas',
        password: 'abbas2021',
        database: 'abbasj_project3',
        connectionLimit: 10
    })

    return connection;
   }

   return _connection;

}


export async function sendQuery(query: string){
  return (await _connection).query(query);
}