declare var require: any
var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { GraphQLObjectType, GraphQLSchema, GraphQLInt, GraphQLString, GraphQLList, GraphQLBoolean } = require('graphql');

import {sendQuery} from "./database"
// import * as cors from 'cors';

const MovieType = new GraphQLObjectType({
    name: "Movie",
    fields: () => ({
        id: { type: GraphQLString },
        title: { type: GraphQLString },
        genre: { type: GraphQLString },
        rating_dice_throw: { type: GraphQLInt },
        year: { type: GraphQLString },
        director_first_name: { type: GraphQLString },
        director_last_name: { type: GraphQLString },
        description: { type: GraphQLString },
        cover_image: { type: GraphQLString }
    })
})

const RootQuery = new GraphQLObjectType({
    name: "RootQueryType",
    description: 'Root query',
    fields: {
        getAllMovies: {
            type: new GraphQLList(MovieType),
            args: {
                lim: {type: GraphQLInt}
            },
            async resolve(parent: any, args: any) {
                const response = await sendQuery('SELECT * FROM movie LIMIT '+args.lim);
                return response[0];
            }
        },
        getMovieByColumnType: {
            type: new GraphQLList(MovieType),
            args: {  
                id: { type: GraphQLInt },
                title: { type: GraphQLString },
                genre: { type: GraphQLString },
                rating_dice_throw: { type: GraphQLInt },
                year: { type: GraphQLString },
                director_first_name: { type: GraphQLString },
                director_last_name: { type: GraphQLString },
                description: { type: GraphQLString },
            },
            async resolve(parent: any, args: any) {
                let query = '';
                if(typeof args.id!='undefined' && args.id){query = getQuery('id' , args.id)}
                else if(typeof args.title!='undefined' && args.title){query = getQuery('title' , args.title)}
                else if(typeof args.genre!='undefined' && args.genre){query = getQuery('genre' , args.genre)}
                else if(typeof args.rating_dice_throw!='undefined' && args.rating_dice_throw){query = getQuery('rating_dice_throw' , args.rating_dice_throw)}
                else if(typeof args.year!='undefined' && args.year){query = getQuery('year' , args.year)}
                else if(typeof args.director_first_name!='undefined' && args.director_first_name){query = getQuery('director_first_name' , args.director_first_name)}
                else if(typeof args.director_last_name!='undefined' && args.director_last_name){query = getQuery('director_last_name' , args.director_last_name)}
                else if(typeof args.description!='undefined' && args.description){query = getQuery('description' , args.description)}

                const response = await sendQuery(query);
                return response[0];
            }
        },
        getMoviesBySearch: {
            type: new GraphQLList(MovieType),
            args: {
                id: {type: GraphQLInt},
                title: { type: GraphQLString },
                genre: { type: GraphQLString },
                rating_dice_throw: { type: GraphQLInt },
                year: { type: GraphQLInt },
                description: { type: GraphQLString },
                order: {type: GraphQLBoolean}
            },
            async resolve(parent: any, args: any) {
                let query = '';
                if(typeof args.title!='undefined'  && typeof args.genre!='undefined'){
                    query = getSearchQuery(args.title, args.genre, args.order)}
                const response = await sendQuery(query);
                return response[0];
            }
        }
    }
})

function getQuery(key: string, value: string){
    return 'SELECT * FROM movie WHERE ' + key + ' = "'  + value + '"'
}

// builds specialized query for searching with filtering
function getSearchQuery(title: string, genre: string, order: boolean): string {
    let query = 'SELECT * FROM movie'
    
    if (title != '') { query += ' WHERE title LIKE "%' + title + '%"' }
    if (title != '' && genre != '') { query += ' AND' }
    if (title == '' && genre != '') { query += ' WHERE' }
    if (genre != '') { query += ' genre LIKE "%' + genre + '%"' }
    if (order) { query += ' ORDER BY year DESC' }

    console.log(query)
    return query
}

const Mutation = new GraphQLObjectType({
    name: "Mutation",
    description: 'This is for creating a movie',
    fields: {
        createMovie: {
            type: MovieType,
            args: {
                id: {type: GraphQLInt},
                title: { type: GraphQLString },
                genre: { type: GraphQLString },
                rating_dice_throw: { type: GraphQLInt },
                year: { type: GraphQLInt },
                director_first_name: { type: GraphQLString },
                director_last_name: { type: GraphQLString },
                description: { type: GraphQLString },
                cover_image: { type: GraphQLString }
            },
            async resolve(parent: any, args: any) { 
                const response = await sendQuery('INSERT INTO movie VALUES (${args.id}, ${args.title}, ${args.genre}, ${args.rating_dice_throw}, ${args.year}, ${args.director_first_name}, ${args.director_last_name}, ${args.description}, ${args.cover_image})');
                return response[0];
            }
        },
        deleteMovie: {
            type: MovieType,
            args: {
                id: {type: GraphQLInt}
            },
            async resolve(parent: any, args: any) { 
                const response = await sendQuery('DELETE FROM movie WHERE id = ${args.id}');
                return response[0];
            }
        },
        updateMovie: {
            type: MovieType,
            args: {
                id: {type: GraphQLInt},
                title: { type: GraphQLString },
                genre: { type: GraphQLString },
                rating_dice_throw: { type: GraphQLInt },
                year: { type: GraphQLInt },
                director_first_name: { type: GraphQLString },
                director_last_name: { type: GraphQLString },
                description: { type: GraphQLString },
                cover_image: { type: GraphQLString }
            },
            async resolve(parent: any, args: any) { 
                const response = await sendQuery('UPDATE movie SET title = ${args.title}, genre = ${args.genre}, rating_dice_throw = ${args.rating_dice_throw}, year = ${args.year}, director_first_name = ${args.director_first_name}, director_last_name = ${args.director_last_name} , description = ${args.description}, cover_image = ${args.cover_image} WHERE id = ${args.id}');
                return response[0];
            }
        },
        updateRating: {
            type: MovieType,
            args: {
                id: {type: GraphQLInt},
                rating_dice_throw: { type: GraphQLInt }
            },
            async resolve(parent: any, args: any) { 
                const response = await sendQuery('UPDATE movie SET rating_dice_throw =  ${args.rating_dice_throw} WHERE id = ${args.id}');
                return response[0];
            }
        }
    }
})

const schema = new GraphQLSchema({query: RootQuery, mutation: Mutation})


var app = express();

// Add headers
app.use(function (req: any, res: any, next: any) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});


// app.use(cors());

app.use('/graphql', graphqlHTTP({
    schema: schema,
    graphiql: true,
}));

app.listen(8080, () => console.log('Server running on port 8080'));
